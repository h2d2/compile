#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os
import re

import tools.call    as tl_call
import tools.fs      as tl_fs
import tools.url     as tl_url

LOGGER = logging.getLogger("H2D2.compile.mpi")

def openmpi_wget(mpiVer) :
    """
    Download openmpi
    """
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "openmpi_wget"]))

    mpiDone = False
    for i in range(9, 0, -1):
        if not mpiDone:
            mpiFic = "openmpi-%s.%d.tar.gz" % (mpiVer, i)
            if os.path.isfile(mpiFic):
                LOGGER.debug("OpenMPI file found: %s", mpiFic)
                mpiDone = True
    if not mpiDone:
        LOGGER.debug("OpenMPI: no files found, downloading")
    for i in range(9, 0, -1):
        if not mpiDone:
            mpiFic = "openmpi-%s.%d.tar.gz" % (mpiVer, i)
            mpiUrl = "https://download.open-mpi.org/release/open-mpi/v%s/%s" % (mpiVer, mpiFic)
            if  tl_url.urlExist(mpiUrl):
                isOk = tl_url.urlDownload(mpiUrl)
                if not isOk:
                    LOGGER.error("Warning: Could not download OpenMPI")
                    LOGGER.error("   URL: %s", mpiUrl)
                    exit(1)
                mpiDone = True

    return mpiFic if mpiDone else None

def openmpi_i4_inc(mpiDir, mpiVer):
    """
    Get the stock i4 openmpi file directory (include files)
    """
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "openmpi_i4_inc"]))
    mpiInt = None

    # ---  Check if bin in same directory
    mpiH = os.path.join(mpiDir, "include", "mpi.h")
    if os.path.isfile(mpiH):
        mpiInt = 'int'

    # ---  Search mpi.h with "ompi_fortran_integer_t int" in INCLUDE dir
    if not mpiInt:
        LOGGER.dump("Searching mpicc includes for mpi.h")
        regIncs = re.compile(r"-I(?P<inc>\S*)")
        regPtrn = re.compile(r"(\s*ompi_fortran_integer_t\s*)(?P<int>[a-z]+)")
        ret = tl_call.doCallOrRaise(["mpicc", "-showme"], doLog=False)
        for mpiI in re.finditer(regIncs, ret):
            LOGGER.dump("   %s", mpiI['inc'])
            mpiH = os.path.join(mpiI['inc'], "mpi.h")
            if os.path.isfile(mpiH):
                with open(mpiH, 'r') as f:
                    match = re.search(regPtrn, f.read())
                    if match:
                        mpiInt = match.group('int')
                        if mpiInt == 'int':
                            break

    # ---  Search mpi.h with "ompi_fortran_integer_t int"
    if not mpiInt:
        LOGGER.dump("Exhaustive search for mpi.h")
        vMaj, vMin, vRel = mpiVer.split('.')
        ficPtrn = os.path.join("include", "mpi.h").replace('\\', '\\\\')
        regVMaj = re.compile(r"(\s*OMPI_MAJOR_VERSION\s*)(?P<val>[0-9]+)")
        regVMin = re.compile(r"(\s*OMPI_MINOR_VERSION\s*)(?P<val>[0-9]+)")
        regVRel = re.compile(r"(\s*OMPI_RELEASE_VERSION\s*)(?P<val>[0-9]+)")
        regPtrn = re.compile(r"(\s*ompi_fortran_integer_t\s*)(?P<int>[a-z]+)")
        for sDir in ['/usr', '/opt', '/home', '/']:
            for mpiH in tl_fs.locate(ficPtrn, sDir):
                LOGGER.dump("   %s", mpiH)
                with open(mpiH, 'r') as f:
                    lines = f.read()
                    matchPtrn = re.search(regPtrn, lines)
                    matchVMaj = re.search(regVMaj, lines)
                    matchVMin = re.search(regVMin, lines)
                    matchVRel = re.search(regVRel, lines)
                    if matchPtrn and matchVMaj and matchVMin and matchVRel:
                        if (matchVMaj['val'] == vMaj and
                            matchVMin['val'] == vMin and
                            matchVRel['val'] == vRel):
                            mpiInt = matchPtrn.group('int')
                            if mpiInt == 'int':
                                break
            if mpiInt == 'int':
                break

    # ---  Get openmpi directory
    if mpiInt != 'int':
        LOGGER.error("Could not detect a valid openmpi INTEGER*4 install in")
        for mpiH in tl_fs.locate(ficPtrn, "/"):
            LOGGER.error("   %s", mpiH)
        return None

    # ---  Get openmpi directory
    if mpiInt == 'int':
        mpiDir = os.path.dirname(mpiH)
        mpiDir = os.path.dirname(mpiDir)
        LOGGER.debug('"include/mpi.h" found in %s', mpiDir)
        return mpiDir

def openmpi_i4_bin(mpiDir):
    """
    Get the stock i4 openmpi bin directory
    """
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "openmpi_i4_bin"]))

    # ---  Check if bin in same directory
    mpiBin = os.path.join(mpiDir, "bin")
    if os.path.isdir(mpiBin):
        return mpiBin

    mpiInt = None
    # ---  Search for bin directory
    ficPtrn = os.path.join("bin", "ompi_info").replace('\\', '\\\\')
    regPtrn = re.compile(r"(\s*Fort integer size:\s*)(?P<int>[0-9]+)")
    for mpiO in tl_fs.locate(ficPtrn, "/"):
        ret = tl_call.doCallOrRaise([mpiO, "-all"], doLog=False)
        match = re.search(regPtrn, ret)
        if match:
            mpiInt = match.group("int")
            if mpiInt == "4":
                break

    # ---  Get openmpi directory
    if mpiInt == "4":
        mpiBin = os.path.dirname(mpiO)
        return mpiBin
    else:
        LOGGER.error("Could not detect a valid openmpi INTEGER*4 bin in")
        for mpiO in tl_fs.locate(ficPtrn, "/"):
            LOGGER.error("   %s", mpiO)
        return None

def openmpi_i4_version():
    """
    Get the stock i4 openmpi version and bin dir
    """
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "openmpi_i4_version"]))
    mpiInt = None

    # ---  First, search for bin in PATH
    ficPtrn = "ompi_info"
    regPtrn = re.compile(r"(\s*Fort integer size:\s*)(?P<int>[0-9]+)")
    try:
        mpiO = ficPtrn
        ret = tl_call.doCallOrRaise([mpiO, "-all"], doLog=False)
        match = re.search(regPtrn, ret)
        if match:
            mpiInt = match.group("int")
    except:
        pass

    # ---  Then, search for bin directory
    if not mpiInt:
        ficPtrn = os.path.join("bin", "ompi_info").replace('\\', '\\\\')
        for sDir in ['/usr', '/opt', '/home', '/']:
            for mpiO in tl_fs.locate(ficPtrn, sDir):
                ret = tl_call.doCallOrRaise([mpiO, "-all"], doLog=False)
                match = re.search(regPtrn, ret)
                if match:
                    mpiInt = match.group("int")
                    if mpiInt == "4":
                        break
            if mpiInt == "4":
                break

    # ---  Raise if not found
    if mpiInt != "4":
        LOGGER.critical("Could not detect a valid openmpi INTEGER*4 version in")
        for mpiO in tl_fs.locate(ficPtrn, "/"):
            LOGGER.critical("   %s", mpiO)
        raise RuntimeError("Could not detect a valid openmpi INTEGER*4 version")

    # ---  Get openmpi dir
    ret = tl_call.doCallOrRaise(['which', mpiO], doLog=False)
    ret = os.path.dirname(ret)
    mpiDir = os.path.dirname(ret)
    LOGGER.debug("MPI detected in %s", mpiDir)

    # ---  Get openmpi version
    regPtrn = re.compile(r"(\s*Open MPI:\s*)(?P<ver>[0-9\.]+)")
    ret = tl_call.doCallOrRaise(mpiO, doLog=False)
    match = re.search(regPtrn, ret)
    mpiVer = match.group("ver")
    LOGGER.debug("MPI version %s", mpiVer)

    return mpiDir, mpiVer

def openmpi_install_i4(mpiTgt, mpiDir, mpiVer):
    """
    Install openmpi in mpiTgt
    """
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "openmpi_install_i4"]))

    mpiInc = openmpi_i4_inc(mpiDir, mpiVer)
    mpiBin = openmpi_i4_bin(mpiDir)
    if not os.path.isdir(mpiTgt):
        os.mkdir(mpiTgt)

    with tl_fs.pushd(mpiTgt):
        tl_fs.updateSymlink(os.path.join(mpiInc, "include"), "include")
        if os.path.isdir(os.path.join(mpiInc, "lib")):
            tl_fs.updateSymlink(os.path.join(mpiInc, "lib"), "lib")
        elif os.path.isdir(os.path.join(mpiInc, "lib64")):
            tl_fs.updateSymlink(os.path.join(mpiInc, "lib64"), "lib")
        else:
            raise RuntimeError("Could not detect a valid openmpi lib directory")
        tl_fs.updateSymlink(mpiBin, "bin")

def openmpi_compile_i8 (mpiFic, optDir) :
    """
    Compile openmpi for i8, install in optDir
    """
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "openmpi_compile_i8"]))

    mpiDir = mpiFic
    mpiDir = os.path.splitext(mpiDir)[0]
    mpiDir = os.path.splitext(mpiDir)[0]

    # https://www.open-mpi.org/faq/?category=building#easy-build
    # http://diracprogram.org/doc/release-12/installation/int64/mpi.html
    tl_call.doCallOrRaise(["gunzip -c", mpiFic, "|", "tar xf -"])

    with tl_fs.pushd(mpiDir):
        mpiCmd = [
            "./configure",
            "--prefix=%s" % optDir,
            "--enable-static",
            "FCFLAGS='-m64 -fdefault-integer-8'",
            ]
        LOGGER.debug("OpenMPI configure with command:")
        LOGGER.debug("   %s", " ".join(mpiCmd))
        tl_call.doCallOrRaise(mpiCmd)
        LOGGER.debug("OpenMPI make all")
        tl_call.doCallOrRaise("make all")
        LOGGER.debug("OpenMPI install to %s", optDir)
        tl_call.doCallOrRaise("make install")

def xeq_openmpi(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq_openmpi"]))

    mpiDir, mpiVer = openmpi_i4_version()

    if ctx.integer == "i4":
        mpiOpt = os.path.join(ctx.optDir, "openmpi-%s-unx64" % mpiVer)
        mpiInc = os.path.join(mpiOpt, "include")
        if os.path.isdir(mpiInc):
            LOGGER.info("OpenMPI already up to date in %s", mpiOpt)
        else:
            openmpi_install_i4(mpiOpt, mpiDir, mpiVer)
        mpiLnk =  ctx.MPIlib + "-unx64"
        # mpiInt = "int"
    elif ctx.integer == "i8":
        mpiVer = '.'.join( mpiVer.split('.')[0:2] )
        mpiFic = openmpi_wget(mpiVer)           # openmpi-a.b.c.tar.gz
        mpiOpt = mpiFic
        mpiOpt = os.path.splitext(mpiOpt)[0]    # remove .tar.gz
        mpiOpt = os.path.splitext(mpiOpt)[0]
        mpiOpt = os.path.join(ctx.optDir, "%s-unx64i8" % mpiOpt)
        mpiInc = os.path.join(mpiOpt, "include")
        if os.path.isdir(mpiInc):
            LOGGER.info("OpenMPI already up to date in %s", mpiOpt)
        else:
            openmpi_compile_i8(mpiFic, mpiOpt)
        mpiLnk = ctx.MPIlib + "-unx64i8"
        # mpiInt = "long"
    else:
        raise RuntimeError("Invalid OpenMPI integer size: %s not in [i4, i8]" % ctx.integer)

    if not os.path.islink(mpiLnk):
        LOGGER.debug("Define symlink for %s", mpiLnk)
        os.symlink(mpiOpt, mpiLnk)

def intelmpi_dir():
    """
    Get the IntelMPI directory (include, bin, ...)
    """
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "intelmpi_version"]))

    try:
        mpiRoot = os.environ["I_MPI_ROOT"]
        if os.path.isdir( os.path.join(mpiRoot, "intel64") ):
            mpiRoot = os.path.join(mpiRoot, "intel64")
        mpiExec = os.path.join(mpiRoot, "bin", "mpiexec")
        ret = tl_call.doCallOrRaise(['"%s"' % mpiExec, "--version"], doLog=False)
        if ret.startswith("Intel(R) MPI Library for Windows* OS"):
            return mpiRoot
        elif ret.startswith("Intel(R) MPI Library for Linux* OS"):
            return mpiRoot
        else:
            errMsg = [
                "Not a valid Intel mpiexec: '%s'" % mpiExec,
            ]
            raise RuntimeError("\n".join(errMsg))
    except KeyError as e:
        errMsg = [
            "Could not detect a valid IntelMPI installation",
            "Please download and install from:",
            "https://software.intel.com/en-us/mpi-library/choose-download",
        ]
        raise RuntimeError("\n".join(errMsg))

def xeq_intelmpi(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq_intelmpi"]))

    mpiRoot = intelmpi_dir()

    if ctx.integer == "i4":
        pass
    elif ctx.integer == "i8":
        pass
    else:
        raise RuntimeError("Invalid IntelMPI integer size: %s not in [i4, i8]" % ctx.integer)

def msmpi_dir():
    """
    Get the MSMPI directory (include, bin, ...)
    """
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "msmpi_dir"]))

    try:
        mpiRoot = os.path.normpath( os.path.join(os.environ["MSMPI_INC"], "..") )
        mpiBin  = os.path.join(os.environ["MSMPI_BIN"])
        mpiExec = os.path.join(mpiBin, "mpiexec")
        ret = tl_call.doCallOrRaise('"%s"' % mpiExec, doLog=False)
        if ret[0:29] == "Microsoft MPI Startup Program":
            return mpiRoot
        else:
            errMsg = [
                "Not a valid Microsoft MPI mpiexec: '%s'" % mpiExec,
            ]
            raise RuntimeError("\n".join(errMsg))
    except KeyError as e:
        errMsg = [
            "Could not detect a valid Microsoft MPI SDK installation",
            "Please download and install 'msmpisdk.msi' from:"
            "https://www.microsoft.com/en-us/download/details.aspx?id=57467",
        ]
        raise RuntimeError("\n".join(errMsg))

def xeq_msmpi(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq_msmpi"]))

    mpiRoot = msmpi_dir()

    if ctx.integer == "i4":
        pass
    else:
        raise RuntimeError("Invalid MSMPI integer size: %s not in [i4]" % ctx.integer)


def xeq(ctx):
    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2(): return

    # ---  Header
    LOGGER.info("H2D2 - Define MPI")

    # ---  Pre-condition
    assert ctx.optDir, "ctx.optDir must be defined"
    assert ctx.MPIlib, "ctx.MPIlib must be defined"
    assert ctx.integer,"ctx.integer must be defined"

    with tl_fs.pushd(os.environ["INRS_LXT"]):
        if ctx.MPIlib == "openmpi":
            xeq_openmpi(ctx)
        elif ctx.MPIlib == "msmpi":
            xeq_msmpi(ctx)
        elif ctx.MPIlib == "intelmpi":
            xeq_intelmpi(ctx)

    # ---  Footer
    LOGGER.info("H2D2 - Define MPI: Done")

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    ctx.MPIlib = 'msmpi'
    os.environ['INRS_LXT'] = os.path.join(ctx.baseDir, "external")
    xeq(ctx)
