#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os
import tempfile

import tools.ptf     as tl_ptf
import tools.fs      as tl_fs

LOGGER = logging.getLogger("H2D2.compile.env")

def xeq(ctx):
    # ---  Header
    LOGGER.info("H2D2 - Define environment")

    # ---  Pre-condition
    assert ctx.baseDir, "ctx.baseDir must be defined"

    if not os.path.isdir(ctx.baseDir):
        os.mkdir(ctx.baseDir)
    with tl_fs.pushd(ctx.baseDir):
        if not os.path.isdir("external"):
            os.mkdir("external")

    os.environ['INRS_DEV'] = ctx.baseDir
    os.environ['INRS_BLD'] = os.path.join(ctx.baseDir, "build")
    os.environ['INRS_TBX'] = os.path.join(ctx.baseDir, "toolbox")
    os.environ['INRS_LXT'] = os.path.join(ctx.baseDir, "external")

    hasTmp = 'TMP' in os.environ
    if not hasTmp:
        os.environ['TMP'] = tempfile.gettempdir()

    if tl_ptf.isWindows():
        print()
        print("===")
        print("For future compilation of H2D2 with scons,")
        print("you may want to add the following lines to your ENVIRONMENT")
        print("INRS_DEV=%s" % os.environ['INRS_DEV'])
        print("INRS_BLD=%s" % os.environ['INRS_BLD'])
        print("INRS_TBX=%s" % os.environ['INRS_TBX'])
        print("INRS_LXT=%s" % os.environ['INRS_LXT'])
        if not hasTmp: 
            print("TMP=%s" % os.environ['TMP'])
        print("===")
        print()
    else:
        print()
        print("===")
        print("For future compilation of H2D2 with scons,")
        print("you may want to add the following lines to your .bash_aliases")
        print("export INRS_DEV=%s" % os.environ['INRS_DEV'])
        print("export INRS_BLD=%s" % os.environ['INRS_BLD'])
        print("export INRS_TBX=%s" % os.environ['INRS_TBX'])
        print("export INRS_LXT=%s" % os.environ['INRS_LXT'])
        if not hasTmp: 
            print("export TMP=%s" % os.environ['TMP'])
        print("===")
        print()

    # ---  Footer
    LOGGER.info("H2D2 - Define environment: Done")

if __name__ == "__main__":
    from tools.context     import getTestContext
    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    xeq(ctx)
