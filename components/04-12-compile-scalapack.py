#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os

import tools.ptf     as tl_ptf
import tools.fs      as tl_fs
import tools.call    as tl_call

LOGGER = logging.getLogger("H2D2.compile.build.external.scalapack")

"""https://stackoverflow.com/questions/1158076/implement-touch-using-python"""
def touch(fname, mode=0o666, dir_fd=None, **kwargs):
    flags = os.O_CREAT | os.O_APPEND
    with os.fdopen(os.open(fname, flags=flags, mode=mode, dir_fd=dir_fd)) as f:
        os.utime(f.fileno() if os.utime in os.supports_fd else fname,
            dir_fd=None if os.supports_fd else dir_fd, **kwargs)

def doWork(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "doWork"]))
    lxtLib = "scalapack_installer"

    # ---  MPI dir in external
    if os.path.islink(ctx.MPIlib):
        MPIlnk = ctx.MPIlib
    else:
        if ctx.integer == "i8":
            MPIlnk = ctx.MPIlib+"-unx64i8"
        else:
            MPIlnk = ctx.MPIlib+"-unx64"
    # assert os.path.islink(MPIlnk) or os.path.isdir(MPIlnk)

    # ---  Compile
    flagFic = os.path.join(lxtLib, ".scalapack.compile." + MPIlnk + ".done")
    if not os.path.isfile(flagFic):
        gen_one = "gen_one.bat" if tl_ptf.isWindows() else "./gen_one.sh"
        try:
            tl_call.doCallOrRaise([gen_one, MPIlnk, "static"], cwd=lxtLib)
        except tl_call.CalledProcessError as e:
            print(e.output)
            raise
        tl_call.doCallOrRaise([gen_one, MPIlnk, "dynamic"], cwd=lxtLib)
        touch(flagFic)
    else:
        LOGGER.info("scalapack is detected as up to date.")
        LOGGER.info("To force recompilation, delete file %s", flagFic)

def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))
    lxtLib = "scalapack_installer"

    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2(): return

    # ---  Header
    LOGGER.info("Build external lib: %s", lxtLib)

    # ---  Pre-conditions
    assert os.environ["INRS_LXT"], "INRS_LXT must be defined"
    assert os.environ["INRS_BLD"], "INRS_BLD must be defined"
    assert ctx.MPIlib,  "ctx.MPIlib must be defined"
    assert ctx.integer, "ctx.integer must be defined"

    # ---  Do the job
    with tl_fs.pushd(os.environ["INRS_LXT"]):
        doWork(ctx)

    # ---  Footer
    LOGGER.info("Build external lib: %s: Done", lxtLib)

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    os.environ['INRS_LXT'] = os.path.join(ctx.baseDir, "external")
    xeq(ctx)
