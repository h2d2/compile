#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import shutil
import os

import tools.fs      as tl_fs
import tools.git     as tl_git
import tools.url     as tl_url

LOGGER = logging.getLogger("H2D2.compile.download.external.ParMetis")

def doWork(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "doWork"]))

    # ---  Download files
    pkgDir = "parmetis-4.0.3"
    pkgFic = "%s.tar.gz" % pkgDir
    pkgUrl = "http://glaros.dtc.umn.edu/gkhome/fetch/sw/parmetis"
    if not os.path.isfile(pkgFic):
        isOk = tl_url.urlDownload(pkgUrl+"/"+ pkgFic)
        if not isOk:
            if os.path.isfile(pkgFic): os.remove(pkgFic)
            raise RuntimeError("Failed downloading %s" % pkgFic)

    # ---  untar to directory
    if not os.path.isdir(pkgDir):
        assert os.path.isfile(pkgFic)
        try:
            tl_fs.untarFile(pkgFic)
        except (tl_fs.TarError, EOFError) as e:
            LOGGER.warning("deleting invalid file %s" % pkgFic)
            if os.path.isfile(pkgFic): os.remove(pkgFic)
            if os.path.isdir (pkgDir): shutil.rmtree(pkgDir)
            raise

    # ---  Modify file gk_arch.h
    with tl_fs.pushd(os.path.join(pkgDir, "metis", "GKlib")):
        fic = "gk_arch.h"
        ficOri = fic+".ori"
        ficNew = fic+".new"
        if not os.path.isfile(ficOri):
            lines = []
            with open(fic, "r") as ifs:
                LOGGER.trace("Modify %s in %s" % (fic, os.getcwd()))
                for line in ifs:
                    line = line.rstrip()
                    ls = line.strip()
                    if ls == '#include "ms_stdint.h"':
                        lines.append("  /* --- Modified for recent versions of MSC */")
                        lines.append("  #if (_MSC_VER < 1600)")
                        lines.append("    %s" % line.strip())
                        lines.append("    %s" % next(ifs).strip())
                        lines.append("    %s" % next(ifs).strip())
                        lines.append("  #else")
                        lines.append("    #include <stdint.h>")
                        lines.append("    #include <inttypes.h>")
                        lines.append("  #endif")
                    elif ls == "/* MSC does not have rint() function */":
                        lines.append("  /* --- Modified for recent versions of MSC */")
                        lines.append("  /* MSC may not have rint() function */")
                        lines.append("  #if (_MSC_VER < 1800)")
                        lines.append("    %s" % next(ifs).strip())
                        lines.append("  #endif")
                    elif ls == "/* MSC does not have INFINITY defined */":
                        lines.append("  /* --- Modified for recent versions of MSC */")
                        lines.append("  /* MSC may not have INFINITY defined */")
                        lines.append("  %s" % next(ifs).strip())
                        lines.append("    #if (_MSC_VER < 1900)")
                        lines.append("      %s" % next(ifs).strip())
                        lines.append("    #endif")
                        lines.append("  %s" % next(ifs).strip())
                    else:
                        lines.append(line)

            with open(ficNew, "w") as f:
                f.write("\n".join(lines))
                f.write("\n")

            if os.path.isfile(ficNew):
                os.rename(fic, ficOri)
                os.rename(ficNew, fic)

    # ---  git clone
    gitGrp = "external"
    gitLib = "ParMetis-build"
    gitDir = "/".join([gitGrp, gitLib])
    tl_git.gitLabCloneBranch(gitDir, gitBrn=ctx.branch, cwd=".", defaultToMaster=True)

    # ---  Copy to compile directory
    tl_fs.copytree(gitLib, pkgDir, ignore=shutil.ignore_patterns(".*"))

def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))
    lxtLib = "ParMetis"

    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2(): return

    # ---  Header
    LOGGER.info("Download external lib: %s" % lxtLib)

    # ---  Pre-conditions
    assert os.environ["INRS_LXT"], "INRS_LXT must be defined"

    # ---  Do the job
    with tl_fs.pushd(os.environ["INRS_LXT"]):
        doWork(ctx)

    # ---  Footer
    LOGGER.info("Download external lib: %s: Done", lxtLib)

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    xeq(ctx)
