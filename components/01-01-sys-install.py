#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from bs4 import BeautifulSoup
import logging
import os
import platform
import re
import shutil

import tools.call    as tl_call
import tools.fs      as tl_fs
import tools.ptf     as tl_ptf
import tools.url     as tl_url

LOGGER = logging.getLogger("H2D2.compile.sys-install")

def is_sudo():
    try:
        # Test first with sudo -v
        # sudo -v can have falls positive
        ret = tl_call.doCallOrRaise("sudo -v", doLog=False)
        # Test then with groups
        # groups can be many things
        # not sure to catch all, be we should be on the secure side
        grps = tl_call.doCallOrRaise("groups", doLog=False)
        ret = any(g in ['admin', 'sudo', 'wheel'] for g in grps.split())
    except:
        ret = False
    return ret

def has_apt():
    return shutil.which("apt-get")

def has_yum():
    return shutil.which("which yum")

def has_zypper():
    return shutil.which("which zypper")

def has_git():
    ret = tl_call.doCallOrRaise("git --version", doLog=False)
    if ret[0:12] != "git version ": return False
    return True

def has_tortoisegit():
    pth = os.path.join(os.environ["ProgramFiles"], "TortoiseGit", "bin")
    return os.path.isdir(pth)

def has_gcc():
    try:
        LOGGER.info("Testing for gcc")
        ret = tl_call.doCallOrRaise("gcc --version", doLog=False)
        if not re.match(r'gcc(-[0-9]+)?\ \(', ret): return False
        LOGGER.info("Testing for g++")
        ret = tl_call.doCallOrRaise("g++ --version", doLog=False)
        if not re.match(r'g\+\+(-[0-9]+)?\ \(', ret): return False
        LOGGER.info("Testing for gfortran")
        ret = tl_call.doCallOrRaise("gfortran --version", doLog=False)
        if len(ret) < 13 or ret[:13] != "GNU Fortran (": return False
    except tl_call.CalledProcessError:
        return False
    return True

def has_intel():
    if tl_ptf.isWindows():
        iclext, iclnul, iclver = 'bat', '> NUL', '/QV'
    else:
        iclext, iclnul, iclver = 'sh', '/dev/null', '--version'

    LOGGER.trace("Testing for icl")
    if shutil.which('icl'):
        cmd = 'icl %s' % iclver
        try:
            ret = tl_call.doCallOrRaise(cmd, doLog=False)
            if "Intel(R) C++" not in ret or "Intel(R) 64 Compiler" not in ret: return False
        except tl_call.CalledProcessError as e:
            return False
    else:
        try:
            keys = [ k for k in os.environ if k.startswith('ICPP_COMPILER') ]
            k = sorted(keys)[-1]
            scr = ".".join(("iclvars", iclext))
            pth = os.path.join(os.environ[k], "bin", scr)
            cmd = '"%s" intel64 %s' % (pth, iclnul)
            LOGGER.warning("Intel C++ Compiler is installed but not in PATH")
            LOGGER.warning("Execute Intel configuration script before launching H2D2 compilation")
            LOGGER.warning("   %s" % cmd)
        finally:
            return False

    LOGGER.trace("Testing for ifort")
    if shutil.which('ifort'):
        cmd = 'ifort %s' % iclver
        try:
            ret = tl_call.doCallOrRaise(cmd, doLog=False)
            if "Intel(R) Fortran" not in ret or "Intel(R) 64 Compiler" not in ret: return False
        except tl_call.CalledProcessError as e:
            return False
    else:
        try:
            keys = [ k for k in os.environ if k.startswith('IFORT_COMPILER') ]
            k = sorted(keys)[-1]
            scr = ".".join(("ifortvars", iclext))
            pth = os.path.join(os.environ[k], "bin", scr)
            cmd = '"%s" intel64 %s' % (pth, iclnul)
            LOGGER.warning("Intel Fortran Compiler is installed but not in PATH")
            LOGGER.warning("Execute Intel configuration script before launching H2D2 compilation")
            LOGGER.warning("   %s" % cmd)
            return False
        finally:
            return False

    return True

def installUnxSmarGit(ctx):
    assert not tl_ptf.isWindows()

    if not os.path.isdir(ctx.optDir):
        os.mkdir(ctx.optDir)
    if not os.path.isdir(ctx.dwnDir):
        os.mkdir(ctx.dwnDir)

    # ---  Install SmartGit
    LOGGER.info("Smartgit")
    sgitVer = "smartgit-linux-20_2_1"
    sgitFil = sgitVer + ".tar.gz"
    sgitUrl = "https://www.syntevo.com/downloads/smartgit/" + sgitFil

    # ---  Download
    with tl_fs.pushd(ctx.dwnDir):
        if not os.path.isfile(sgitFil):
            LOGGER.debug("Download %s", sgitUrl)
            isOk = tl_url.urlDownload(sgitUrl)
            if not isOk:
                LOGGER.warning("Warning: Could not download SmartGit")
                LOGGER.warning("   URL: %s", sgitUrl)

    # ---  untar
    with tl_fs.pushd(ctx.optDir):
        if os.path.isfile(sgitFil) and not os.path.isdir(sgitVer):
            LOGGER.debug("untar %s", sgitFil)
            os.mkdir(sgitVer)
            sgitPth = os.path.join(ctx.dwnDir, sgitFil)
            tl_call.doCallOrRaise(["tar","-zxf", sgitPth, "--directory", sgitVer])

    # ---  Sym-links
    with tl_fs.pushd(ctx.optDir):
        if os.path.isdir(sgitVer):
            if os.path.islink("smartgit"):
                os.unlink("smartgit")
            os.symlink(sgitVer, "smartgit")

def installWinCplr(ctx):
    assert tl_ptf.isWindows()
    assert platform.machine() == "AMD64"

    # ---  Mandatory
    LOGGER.info("Mandatory packages")
    if ctx.getCompilerSuite() == "gcc":
        if not has_gcc():
            LOGGER.critical("gcc, g++ and gfortran must be in the PATH")
            exit(1)
    elif ctx.getCompilerSuite() == "itlX64":
        if not has_intel():
            LOGGER.critical("icl, ifort must be in the PATH")
            LOGGER.critical("You may start the scripts from an Intel Compiler Command prompt")
            exit(1)
    else:
        LOGGER.critical("Invalid compiler choice for Windows platform")
        LOGGER.critical("Valid compiler are [gcc, itlX64]")
        exit(1)

def installWinGit(ctx):
    assert tl_ptf.isWindows()

    # ---  Install Git
    if not has_git():
        LOGGER.info("git: download")
        baseUrl = "https://git-scm.com/download/win"
        baseTxt = tl_url.urlContent(baseUrl)
        # ---  Search for iframe that triggers the automatic download
        soup = BeautifulSoup(baseTxt, 'html.parser')
        url = soup.find('iframe')["src"]  # src of only frame
        # ---  Download
        fic = url.split("/")[-1]
        with tl_fs.pushd(ctx.dwnDir):
            if not os.path.isfile(fic):
                LOGGER.info("git: download %s", url)
                tl_url.urlDownload(url)
        # ---  Install
        with tl_fs.pushd(ctx.dwnDir):
            LOGGER.info("git: install %s", fic)
            tl_call.doCallOrRaise(fic)

    # ---  Install TortoiseGit
    if not has_tortoisegit() and not ctx.noGUI:
        LOGGER.info("TortoiseGit: download")
        baseUrl = "https://tortoisegit.org/download/"
        baseTxt = tl_url.urlContent(baseUrl)
        # ---  Search href for 64bit version
        sPat = re.compile(r".*/TortoiseGit-([0-9]+\.)+[0-9]-64bit\.msi")
        soup = BeautifulSoup(baseTxt, 'html.parser')
        url = "https:" + soup.find('a', href=sPat)["href"]
        # ---  Download
        fic = url.split("/")[-1]
        with tl_fs.pushd(ctx.dwnDir):
            if not os.path.isfile(fic):
                LOGGER.info("TortoiseGit: download %s", url)
                tl_url.urlDownload(url)
        # ---  Install
        with tl_fs.pushd(ctx.dwnDir):
            LOGGER.info("TortoiseGit: install %s", fic)
            tl_call.doCallOrRaise(fic)

def installUnx(ctx):
    assert not tl_ptf.isWindows()
    assert platform.machine() == "x86_64"

    pkgs = ["g++", "gfortran", "git"]
    if not is_sudo():
        LOGGER.warning("You are not a sudoer")
        LOGGER.warning("  Make shure the folowing packages are installed:")
        LOGGER.warning("  %s" % ' '.join(pkgs))
        return
    if has_apt():
        installCmd = "sudo apt-get --quiet --assume-yes install"
        LOGGER.debug("install with apt-get")
    elif has_yum():
        installCmd = "sudo yum --assumeyes install"
        LOGGER.debug("install with yum")
        LOGGER.warning("install with yum NEVER tested")
    elif has_zypper():
        installCmd = "sudo zypper --quiet --non-interactive install"
        LOGGER.debug("install with zypper")
        LOGGER.warning("install with zypper NEVER tested")
    else:
        LOGGER.warning("Unknown package installer")
        LOGGER.warning("  Make shure the folowing packages are installed:")
        LOGGER.warning("  %s" % ' '.join(pkgs))
        return

    # ---  Mandatory
    LOGGER.info("Mandatory packages")
    LOGGER.info("   gcc, g++")
    tl_call.doCallOrRaise([installCmd, "gcc", "g++"])
    if ctx.isH2D2():
        LOGGER.info("   gfortran")
        tl_call.doCallOrRaise([installCmd, "gfortran"])
    if ctx.isH2D2() or ctx.isH2D2Remesh():
        LOGGER.info("   make")
        tl_call.doCallOrRaise([installCmd, "make"])
    if ctx.isH2D2():
        LOGGER.info("   openmpi-bin, libopenmpi-dev")
        tl_call.doCallOrRaise([installCmd, "openmpi-bin", "libopenmpi-dev"])
    LOGGER.info("   git")
    tl_call.doCallOrRaise([installCmd, "git"])

    # ---  Useful tools
    if not ctx.noGUI:
        LOGGER.info("Some useful tools")
        LOGGER.info("   geany")
        tl_call.doCallOrRaise([installCmd, "geany"])
        LOGGER.info("   meld")
        tl_call.doCallOrRaise([installCmd, "meld"])
        #tl_call.doCallOrRaise([installCmd, "tree"])

        installUnxSmarGit(ctx)

def installWin(ctx):
    assert tl_ptf.isWindows()
    assert platform.machine() == "AMD64"

    if ctx.isH2D2():
        installWinCplr(ctx)

    installWinGit(ctx)

def xeq(ctx):
    # ---  Header
    LOGGER.info("H2D2 - System")

    # ---  Pre-condition
    assert ctx.target, "ctx.target must be defined"
    assert ctx.optDir, "ctx.optDir must be defined"
    assert ctx.dwnDir, "ctx.dwnDir must be defined"

    # ---  Install packages
    if tl_ptf.isWindows():
        installWin(ctx)
    else:
        installUnx(ctx)

    # ---  Footer
    LOGGER.info("H2D2 - System: Done")

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    xeq(ctx)
