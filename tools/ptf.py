#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

# Utility functions related to: 
#   platform

import platform

def isWindows():
    return platform.system() == 'Windows'

def isUnix():
    return platform.system() == 'Linux'

def getPlatformName():
    if isWindows():
        if platform.architecture()[0] == '32bit': return 'win32'
        if platform.architecture()[0] == '64bit': return 'win64'
    elif isUnix():
        if platform.architecture()[0] == '32bit': return 'unx32'
        if platform.architecture()[0] == '64bit': return 'unx64'
    raise RuntimeError('Invalid platform: %s - %s' % (platform.system(), platform.architecture()[0]))
